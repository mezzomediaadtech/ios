    //
    //  ViewController.m
    //  ManAdSample
    //
    //  Created by MezzoMedia on 2018. 01.
    //  Copyright © 2018년 MezzoMedia. All rights reserved.
    //

#import "ViewController.h"
#import "ManBannerAdViewController.h"
#import "ManInterstitialAdViewController.h"
#import "ManMovieAdViewController.h"
#import "ManFlexibleAdInterstitialViewController.h"
@implementation ViewController
@synthesize scrollview = _scrollview;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self makeTitleLabel];
    self.view.backgroundColor = [UIColor blackColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self makeMenu];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [_scrollview release];
    [super dealloc];
}

-(void)makeMenu{
    UIButton* btn;

    /* 광고 버튼 생성 */
    btn = [self buttonCreateUnderBtn:nil title:@"띠" action:@selector(manBannerAdButton:) gap:10 posY:10];
    btn = [self buttonCreateUnderBtn:btn title:@"전면" action:@selector(manInterstitialAdButton:) gap:10 posY:0];
    btn = [self buttonCreateUnderBtn:btn title:@"동영상" action:@selector(manMovieAdButton:) gap:10 posY:0];

    
    CGFloat w =[UIScreen mainScreen].bounds.size.width;
    CGFloat h =btn.frame.origin.y+btn.frame.size.height+55;
    [self scrollviewInitWithWidth:w Height:h];
}

    /* Publisher, Media, Section 번호*/

    //배너
- (void)manBannerAdButton:(id)sender {
    [self requestManBannerWithPid:@"100" Mid:@"200" Sid:@"300" Rich:YES Ani:YES ];
}
    //테스트_전면
- (void)manInterstitialAdButton:(id)sender {
    [self requestManInterWithPid:@"1504" Mid:@"31778" Sid:@"803888" Ani:YES viewType:@"2"];
}
    //테스트_동영상
- (void)manMovieAdButton:(id)sender {
    [self requestManMovieWithPid:@"100" Mid:@"200" Sid:@"802998" Cate:@"" Ani:YES];
}
 
    /* Setting */
-(void)requestManBannerWithPid:(NSString*)p Mid:(NSString*)m Sid:(NSString*)s Rich:(Boolean)isrich Ani:(Boolean)isAni{
        //배너
    ManBannerAdViewController *manBannerAdViewController = [[ManBannerAdViewController alloc] init];
    [manBannerAdViewController setPID:p MID:m SID:s rich:isrich];
    [self.navigationController pushViewController:manBannerAdViewController animated:isAni];
    [manBannerAdViewController release];
}
-(void)requestManInterWithPid:(NSString*)p Mid:(NSString*)m Sid:(NSString*)s Ani:(Boolean)isAni viewType:(NSString*)viewType{
        //전면
    ManInterstitialAdViewController *manInterstitialAdViewController = [[ManInterstitialAdViewController alloc] init];
    [manInterstitialAdViewController setPID:p MID:m SID:s];
    [manInterstitialAdViewController setViewStyle:viewType];
    [self.navigationController pushViewController:manInterstitialAdViewController animated:isAni];
    [manInterstitialAdViewController release];
}
-(void)requestManFlexibleWithPid:(NSString*)p Mid:(NSString*)m Sid:(NSString*)s Ani:(Boolean)isAni{
        //종료
    ManFlexibleAdInterstitialViewController *manFlexibleAdInterstitialViewController = [[ManFlexibleAdInterstitialViewController alloc] init];
    [manFlexibleAdInterstitialViewController setPID:p MID:m SID:s];
    [self.navigationController pushViewController:manFlexibleAdInterstitialViewController animated:isAni];
    [manFlexibleAdInterstitialViewController release];
}
-(void)requestManMovieWithPid:(NSString*)p Mid:(NSString*)m Sid:(NSString*)s Cate:(NSString*)cate Ani:(Boolean)isAni{
        //무비
    ManMovieAdViewController *manMovieAdViewController = [[ManMovieAdViewController alloc] init];
    [manMovieAdViewController setPID:p MID:m SID:s categoryCD:cate];
    [self.navigationController pushViewController:manMovieAdViewController animated:isAni];
    [manMovieAdViewController release];
}
-(void)scrollviewInitWithWidth:(CGFloat)w Height:(CGFloat)h{
    if(_scrollview!=nil){
        _scrollview.frame = CGRectMake(0, 0, w, h);
        _scrollview.contentSize = CGSizeMake(w, h);
    }
}
-(UIButton*)buttonCreateUnderBtn:(UIButton*)underBtn title:(NSString*)title action:(SEL)action gap:(CGFloat)gap posY:(CGFloat)posY{
    CGFloat buttonWidth = [UIScreen mainScreen].bounds.size.width-10 ;
    CGFloat buttonHeight = [UIScreen mainScreen].bounds.size.height/5;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    CGFloat x =([UIScreen mainScreen].bounds.size.width - buttonWidth) / 2;
    CGFloat y = (underBtn!=nil)?(underBtn.frame.origin.y +underBtn.frame.size.height+gap):posY;
    CGFloat w = buttonWidth;
    CGFloat h = buttonHeight;
    [btn setFrame:CGRectMake(x,y,w,h)];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor grayColor];
    btn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [btn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [self.scrollview addSubview:btn];
    return btn;
}
-(void)makeTitleLabel{
    NSBundle *bundle = [NSBundle mainBundle];
    NSDictionary *info = [bundle infoDictionary];
    NSString *productName = [info objectForKey:@"CFBundleDisplayName"];
    self.title = productName;
}

@end
