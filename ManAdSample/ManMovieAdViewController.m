    //
    //  ManMovieAdViewController.m
    //  ManAdView
    //
    //  Created by MezzoMedia on 2018. 01.
    //  Copyright © 2018년 MezzoMedia. All rights reserved.
    //

#import "ManMovieAdViewController.h"
#import "UIView+Toast.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@implementation ManMovieAdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    if (self) {
            // Custom initialization
    }

    return self;
}


-(void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s categoryCD:(NSString*)cate{
    publisher= [p retain];
    media= [m retain];
    section= [s retain];
    categoryCD = [cate retain];
}

- (void)viewDidLoad{
    [super viewDidLoad];
        // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor blackColor];
    self.title = @"Video Ad";

    curScreenMode = MODE_WIDE;

    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(didEnterBackground:)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];

    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(didBecomeActive:)
                                                 name: UIApplicationDidBecomeActiveNotification
                                               object: nil];
    [self muteSoundActivite:YES];
}

-(void)sizeCheck{
    width = [UIScreen mainScreen].bounds.size.width;
    height = [UIScreen mainScreen].bounds.size.height;
    titleY = self.navigationController.navigationBar.frame.origin.y;
    titleH = self.navigationController.navigationBar.frame.size.height;
    movieY = titleY+titleH;
    height = height-movieY;

    switch (curScreenMode) {
        case MODE_NORMAL: // 4:3
            NSLog(@"MODE_NORMAL 4:3");
            height = ((width / 4) * 3);
            if(height>[UIScreen mainScreen].bounds.size.height - movieY){
                height = [UIScreen mainScreen].bounds.size.height - movieY;
                width =((height / 3) * 4);
            }
            break;
        case MODE_WIDE: // 16:9
            NSLog(@"MODE_NORMAL 16:9");
            height = ((width / 16) * 9);
            if(height>[UIScreen mainScreen].bounds.size.height - movieY){
                height = [UIScreen mainScreen].bounds.size.height - movieY;
                width =((height / 9) * 16);
            }
            break;
        case MODE_STRETCH:
            NSLog(@"MODE_STRETCH");
                //height = height;
            break;
        case MODE_ORIGNAL:
            NSLog(@"MODE_ORIGNAL");
            break;
    }
    movieX = ([UIScreen mainScreen].bounds.size.width - width)/2;
    movieY = (movieY+[UIScreen mainScreen].bounds.size.height - height)/2;
}

- (void)viewDidAppear:(BOOL)animated {
    [self sizeCheck];
    manMovieAdView = [[ManMovieAdView alloc] initWithFrame:CGRectMake(movieX, movieY, width, height)];
    [manMovieAdView setScreenMode:curScreenMode];
    manMovieAdView.delegate = self;
    [manMovieAdView publisherID:publisher mediaID:media sectionID:section];           // Skip
                                                                                      // 카테고리 설정
    manMovieAdView.categoryCD = categoryCD;
    
    // u_age_level (0: 어린이, 1: 청소년 및 성인)<권장 Parameter>
    [manMovieAdView userAgeLevel:@"1"];
    
    // Keyword 타게팅을 위한 함수파라미터 (Optional)
    [manMovieAdView keywordParam:@"KeywordTargeting"];
    
        // 추가정보 설정
    manMovieAdView.gender = @"2";
    manMovieAdView.age = @"15";
    manMovieAdView.userId = @"gun";
    manMovieAdView.userEmail = @"gun@mezzomedia.co.kr";
    manMovieAdView.userPositionAgree = @"1";

        // 랜딩페이지 사파리 이동 설정
    [manMovieAdView useGotoSafari:YES];

    [self.view addSubview:manMovieAdView];
    [manMovieAdView startMovieAd];

    [super viewDidAppear:animated];
}

-(void)muteSoundActivite:(BOOL)check{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:check error:nil];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [self sizeCheck];
    if (manMovieAdView) {
        manMovieAdView.frame = CGRectMake(movieX, movieY, width, height);
    }
    if(clientPlayer){
        [[clientPlayer view] setFrame:CGRectMake(movieX, movieY, width, height)];
    }
}

- (void)viewWillLayoutSubviews {
        //    NSLog(@"[ManMovieAdViewController] ============ viewWillLayoutSubviews");
}

    //- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    //    return (toInterfaceOrientation == UIInterfaceOrientationIsPortrait(toInterfaceOrientation));
    //}
    //
    //- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //    return UIInterfaceOrientationMaskPortrait;
    //}
    //
    //- (BOOL) shouldAutorotate {
    //    return NO;
    //}
    //
    //- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    //    return UIInterfaceOrientationPortrait;
    //}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}


- (void)dealloc {
    NSLog(@"[ManMovieAdViewController] ============ dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];

        // S-Plus 광고 제거
    [self removeManAdView];

    [clientPlayer stop];
    [clientPlayer release];
    [super dealloc];
}

    // S-Plus 광고제거
- (void)removeManAdView {
    if(manMovieAdView){
        [manMovieAdView removeFromSuperview];
        manMovieAdView.delegate = nil;
        [manMovieAdView release];
        manMovieAdView = nil;
    }
}

- (void)startClientMovie {
        // S-Plus 광고제거
    [self removeManAdView];

    NSLog(@"[ManMovieAdViewController] ============ startClientMovie");

    if (clientPlayer == nil) {
        NSString *movieUrl = @"http://vod.midas-i.com/20140708174154_middle.mp4";
        if (movieUrl && [movieUrl length] > 0) {
            [self sizeCheck];
            clientPlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:movieUrl]];
            [clientPlayer setMovieSourceType:MPMovieSourceTypeUnknown];
            [[clientPlayer view] setFrame:CGRectMake(movieX, movieY, width, height)];
            clientPlayer.scalingMode = MPMovieScalingModeAspectFit;
            clientPlayer.controlStyle = MPMovieControlStyleNone;
            [clientPlayer prepareToPlay];
            [clientPlayer play];
            [self.view addSubview:clientPlayer.view];
        }
    }
}

#pragma mark notification

- (void)didEnterBackground:(NSNotification*)notification {
    NSLog(@"[ManMovieAdViewController] ================== didEnterBackground");

}

- (void)didBecomeActive:(NSNotification*)notification {
    NSLog(@"[ManMovieAdViewController] ================== didBecomeActive");

    if (clientPlayer) {
        [clientPlayer play];
    }
}

#pragma mark buttons

- (void)changeScreenButton:(id)sender {

    if (curScreenMode == MODE_NORMAL) {
        curScreenMode = MODE_WIDE;
    } else if (curScreenMode == MODE_WIDE) {
        curScreenMode = MODE_STRETCH;
    } else if (curScreenMode == MODE_STRETCH) {
        curScreenMode = MODE_ORIGNAL;
    } else if (curScreenMode == MODE_ORIGNAL) {
        curScreenMode = MODE_WIDE;
    }
    [manMovieAdView setScreenMode:curScreenMode];
}

#pragma mark ManMovieAdDelegate

/* 동영상 광고 정보 수신 성공
 */
- (void)didReceiveMovieAd:(ManMovieAdView*)manMovieAd {
    
    //SString *log = @"성공";
    NSString *log = @"Success";
    [self.navigationController.view makeToast:log];

    NSLog(@"[ManMovieAdViewController] ================== didReceiveMovieAd");
}

/* 동영상 광고 수신 에러
 */
- (void)didFailReceiveMovieAd:(ManMovieAdView*)manMovieAd errorType:(NSInteger)errorType {
    NSLog(@"[ManMovieAdViewController] ================== didFailReceiveMovieAd - error : %ld", (long)errorType);
    NSString *log = @"none";
    switch (errorType) {
        case NewManAdSuccess:
            //log = @"성공";
            log = @"Success";
            break;

        case NewManAdRequestError:
            //log = @"잘못된 광고 요청";
            log = @"RequestError";
            break;

        case NewManAdParameterError:
            //log = @"잘못된 파라메터 전달";
            log = @"ParameterError";
            break;

        case NewManAdIDError:
            //log = @"광고 미존재";
            log = @"AdIDError";
            break;

        case NewManAdNotError:
            //log = @"광고 없음 (No Ads)";
            log = @"No Ads";
            break;

        case NewManAdServerError:
            //log = @"광고서버 에러";
            log = @"ServerError";
            break;

        case NewManAdNetworkError:
            //log = @"네트워크 에러";
            log = @"NetworkError";
            break;

        case NewManAdCreativeError:
            //log = @"광고물 요청 실패";
            log = @"AdCreativeError";
            break;
    }

    log = [log stringByAppendingString:[NSString stringWithFormat:@" : %ld", errorType]];

    [self.navigationController.view makeToast:log];
}

/* 동영상 광고 종료
 */
- (void)didFinishMovieAd:(ManMovieAdView*)manMovieAd {
    NSLog(@"[ManMovieAdViewController] ================== didFinishMovieAd");
    [self startClientMovie];
}

/* 동영상 광고 플래이 종료 (동영상 광고 플래이 도중 종료)
 */
- (void)didStopMovieAd:(ManMovieAdView*)manMovieAd {
    NSLog(@"===== ManMovieAdViewController - didStopMovieAd!");
    [self removeManAdView];
}

/* 동영상 광고 스킵
 */
- (void)didSkipMovieAd:(ManMovieAdView*)manMovieAd {
    NSLog(@"[ManMovieAdViewController] ================== didSkipMovieAd");
    [self startClientMovie];
}

/* 동영상 광고 클릭
 */
- (void)didClickMovieAd:(ManMovieAdView*)manMovieAd {
    NSLog(@"[ManMovieAdViewController] ================== didClickMovieAd");
        // * 참고 : 현재 시점에서 SPlus(MAN) 광고뷰가 제거 될 경우, didCloseRandingPage delegate함수를 호출 받을 수 없음.

    [self startClientMovie];
}

/* 동영상 광고 랜딩페이지 닫기버튼 클릭
 */
- (void)didCloseRandingPage:(ManMovieAdView*)manMovieAd {
    NSLog(@"[ManMovieAdViewController] ================== didCloseMovieAdRandingPage");
    [self startClientMovie];
}

/* 동영상 광고 없음
 */
- (void)didNotExistMovieAd:(ManMovieAdView*)manMovieAd {

    NSLog(@"[ManMovieAdViewController] ================== didNotExistMovieAd");
    [self startClientMovie];
}

#pragma mark MezzoMedia

/* 동영상 광고 URL 전달 - requestMovieAdURL호출에 대한 콜백
 */
- (void)responseMovieAdURL:(ManMovieAdView*)manMovieAd movieAdURL:(NSString*)movieAdURL {
    
    NSLog(@"[ManMovieAdViewController] ================== movieAdURL : %@", movieAdURL);
}



@end
