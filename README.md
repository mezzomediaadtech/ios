# Library Path
- ManAdSample/ManAdLibrary

# Library List
- ADBanner.h
- JSONKit.h
- JSONKit.m
- libManAdView.a
- ManAdDefine.h
- ManAdViewResources_Simulator.bundle
- ManAdViewResources.bundle
- ManBreakAdInterstitial.h
- ManContentAdView.h
- ManInterstitial.h
- ManMovieAdView.h
- UIView+Toast.h


# Update List
* 19.09.20 : Add user age level func, keyword parameter func